package com.example.weatherlive.activities

import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.weatherlive.BuildConfig
import com.example.weatherlive.R
import com.example.weatherlive.helpers.WLApplication
import com.example.weatherlive.helpers.WLNetworkCallback
import com.example.weatherlive.helpers.WeatherReportResponse
import retrofit2.Call
import retrofit2.Response
import java.io.IOException


class WLHomeActivity : WLActivity() {
    private var tvResultCity: TextView? = null
    private var tvTemperature: TextView? = null
    private var tvHumidity: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wl_activity_home)
        val tvCity = findViewById<AutoCompleteTextView>(R.id.tv_city)
        tvResultCity = findViewById<TextView>(R.id.tv_result_city)
        tvTemperature = findViewById<TextView>(R.id.tv_temperature)
        tvHumidity = findViewById<TextView>(R.id.tv_humidity)

        val ivSearch = findViewById<ImageView>(R.id.iv_search)

        ivSearch.setOnClickListener{
              val city = tvCity.text.toString()
            if(city.length==0){
                dismissKeyboard(tvCity)
                Toast.makeText(
                    this,
                    getString(R.string.enter_the_city),
                    Toast.LENGTH_SHORT
                ).show()
            }else {
                dismissKeyboard(tvCity)
                getLatLng(city)
            }
        }
    }

    private fun getLatLng(city:String){
        if (Geocoder.isPresent()) {
            try {
                val geocoder = Geocoder(this)
                val addresses: List<Address> =
                    geocoder.getFromLocationName(city, 1)
                if(addresses.size>0){
                    val address = addresses.get(0)
                    getForklift(address.latitude, address.longitude,city)
                }else{
                    Toast.makeText(this, getString(R.string.invalid_address),
                        Toast.LENGTH_SHORT).show()
                }

            } catch (ioException: IOException) {
                Log.e("WLHomeActivity", "getLatLng: Exception: " + ioException.localizedMessage)
            }
        }else{
            Toast.makeText(this, getString(R.string.mobile_imcompatible),
                Toast.LENGTH_SHORT).show()
        }
    }
    private fun getForklift (lat : Double, long : Double,city : String) {
        val app: WLApplication = WLApplication.getInstance();
        showProgress()
        WLApplication.getInstance().getAPIManager()?.getWeather(lat,long,BuildConfig.WEATHER_KEY,
             object : WLNetworkCallback<WeatherReportResponse?> {
                override fun onNoNetwork() {
                    dismissProgress()
                    Toast.makeText(
                        app,
                        getString(R.string.internet_connection_offline),
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onResponse(
                    call: Call<WeatherReportResponse?>,
                    response: Response<WeatherReportResponse?>) {
                    dismissProgress()
                    if (response.isSuccessful()) {
                        val weatherReportResponse : WeatherReportResponse? = response.body()
                        tvResultCity?.setText(city);
                        if(weatherReportResponse==null){
                            Toast.makeText(app, getString(R.string.details_not_found),
                                Toast.LENGTH_SHORT).show()
                        }else {
                            var weatherReport = weatherReportResponse.getWeatherReport();
                            tvTemperature?.setText(weatherReport?.getTemp().toString()+" \u00B0");
                            tvHumidity?.setText(weatherReport?.getHumidity().toString()+"%");
                        }
                    }else{
                        Toast.makeText(app, getString(R.string.details_not_found),
                            Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<WeatherReportResponse?>, t: Throwable) {
                    dismissProgress()
                    Toast.makeText(app, getString(R.string.details_not_found), Toast.LENGTH_SHORT).show()
                    Log.e("WLHomeActivity", "Exception: " + t.localizedMessage)
                }
            })
    }

}