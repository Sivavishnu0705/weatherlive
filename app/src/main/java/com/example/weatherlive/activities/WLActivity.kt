package com.example.weatherlive.activities

import android.content.Context
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.weatherlive.R


open class WLActivity : AppCompatActivity() {
    private var layout: RelativeLayout? = null;
    fun showProgress() {
        if (layout == null) {
            layout = RelativeLayout(this)
            val progressBar = ProgressBar(this, null, R.attr.progressBarStyle)
            progressBar.id = R.id.pb_progress
            progressBar.isIndeterminate = true
            val paramsForProgressBar = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            ) //Params for Progress Bar - which decides where to place the view
            paramsForProgressBar.addRule(RelativeLayout.CENTER_IN_PARENT)
            layout!!.addView(progressBar, paramsForProgressBar)
            val paramsForLinearLayout = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            ) //Params for Relative Layout - which decides where to place the view
            paramsForLinearLayout.gravity = Gravity.CENTER
            addContentView(layout, paramsForLinearLayout)
        }
            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            ) //setFlags(flag, mask) - flag: The new window flags which we need to set, mask: Which of the window flag bits to modify.
        layout!!.setVisibility(View.VISIBLE)
    }

    fun dismissProgress() {
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        layout?.setVisibility(View.GONE)
    }

    open fun dismissKeyboard(view: View?) {
        if (view != null) {
            val inputMethodManager: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}