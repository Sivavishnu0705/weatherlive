package com.example.weatherlive.helpers

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class WeatherReport {
    @SerializedName("temp")
    @Expose
    private val temp: Double? = null
    @SerializedName("humidity")
    @Expose
    private val humidity: Int? = null

    fun getTemp(): Double? {
        return temp
    }

    fun getHumidity(): Int? {
        return humidity
    }

}