package com.example.weatherlive.helpers

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class WeatherReportResponse {
    @SerializedName("main")
    @Expose
    private val weatherReport: WeatherReport? = null

    fun getWeatherReport(): WeatherReport? {
        return weatherReport
    }
}