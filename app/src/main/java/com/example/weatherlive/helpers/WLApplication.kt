package com.example.weatherlive.helpers

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.util.Log


class WLApplication : Application() {
    private val TAG = "WHApplication"
    private val mApiManager = WLApiManager()
    companion object {
        private lateinit var sInstance: WLApplication
        /**
         * @return WApplication singleton instance
         */
        fun getInstance(): WLApplication {
            return sInstance
        }
    }

    private val wifiManager: WifiManager? = null
    override fun onCreate() {
        super.onCreate()
        sInstance = this
    }


    override fun onTerminate() {
        super.onTerminate()
    }

    fun getAPIManager(): WLApiManager? {
        return mApiManager
    }

    fun checkNetworkConnection(): Boolean {
        try {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var activeNetwork: NetworkInfo? = null
            if (connectivityManager != null) {
                activeNetwork = connectivityManager.activeNetworkInfo
            }
            if (activeNetwork != null) {
                return activeNetwork.isConnected
            }
        } catch (e: Exception) {
            Log.e(TAG, "checkNetworkConnection: Exception: " + e.localizedMessage)
        }
        return false
    }
}