package com.example.weatherlive.helpers

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log


class WLUtilities {
    companion object {
        private val TAG = "WHUtilities"
        fun checkNetworkConnection(): Boolean {
            try {
                val connectivityManager = WLApplication.getInstance()
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                var activeNetwork: NetworkInfo? = null
                if (connectivityManager != null) {
                    activeNetwork = connectivityManager.activeNetworkInfo
                }
                if (activeNetwork != null) {
                    return activeNetwork.isConnected
                }
            } catch (e: Exception) {
                Log.e(TAG, "checkNetworkConnection: Exception: " + e.localizedMessage)
            }
            return false
        }
    }
}