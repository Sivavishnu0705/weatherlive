package com.example.weatherlive.helpers

import com.example.weatherlive.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


class WLApiManager {
    companion object {
        private val TIMEOUT: Long = 30

        fun getApiInterface():WLApiInterface {
            return getApiInterface(TIMEOUT.toInt())
        }

        fun getApiInterface(timeOut: Int): WLApiInterface {
            return getRetrofitClient().create(WLApiInterface::class.java)
        }

        fun getRetrofitClient() : Retrofit {

            val okClientBuilder = OkHttpClient().newBuilder().
                 readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            okClientBuilder.addInterceptor { chain ->
                val request: Request.Builder = chain.request().newBuilder()
                request.header("Content-Type", "application/json")
                chain.proceed(request.build())
            }
            if (BuildConfig.DEBUG) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                okClientBuilder.addInterceptor(interceptor)
            }

            val authorisedOkHttpClient: OkHttpClient = okClientBuilder.build()
            val gson = GsonBuilder().setLenient().create()
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.WEATHER_BASE_URL)
                .client(authorisedOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
    }

    fun getWeather(
        lat:Double,
        @Query("lon") lon:Double,
        @Query("appid") appId:String,
        callback: WLNetworkCallback<WeatherReportResponse?>
    ) {
        if (WLUtilities.checkNetworkConnection()) {
            val forkliftResponseCall: Call<WeatherReportResponse> =
                WLApiManager.getApiInterface().getWeather(lat,
                     lon,
                    appId)
            forkliftResponseCall.enqueue(callback)
        } else {
            callback.onNoNetwork()
        }
    }

    interface WLApiInterface {
        @GET("weather")
        fun getWeather(@Query("lat") lat:Double,
                       @Query("lon") lon:Double,
                       @Query("appid") appId:String) : Call<WeatherReportResponse>
    }
}