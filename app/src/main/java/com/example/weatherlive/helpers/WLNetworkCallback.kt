package com.example.weatherlive.helpers

import retrofit2.Callback

interface WLNetworkCallback<T> : Callback<T> {
    fun onNoNetwork()
}